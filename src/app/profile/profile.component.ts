import { Component, OnInit } from '@angular/core';
import { Menu } from '../models/menu';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profileMenuItems: Menu[];
  securityMenuItems: Menu[];
  aboutMenuItems: Menu[];
  assetsLocation: string;

  constructor() {}

  ngOnInit(): void {
    this.assetsLocation = '../../../assets/ico/';
    this.initProfileItems();
    this.initSecurityItems();
    this.initAboutItems();
  }

  initProfileItems() {
    this.profileMenuItems = [
      {
        name: 'Personal Details',
        icon: this.assetsLocation + 'personal_details.svg',
        route: ''
      },
      {
        name: 'Inbox',
        icon: this.assetsLocation + 'inbox.svg',
        route: ''
      },
      {
        name: 'Notifications',
        icon: this.assetsLocation + 'notifications.svg',
        route: ''
      },
      {
        name: 'Linked Accounts',
        icon: this.assetsLocation + 'personal_details.svg',
        route: ''
      }
    ];
  }

  initSecurityItems() {
    this.securityMenuItems = [
      {
        name: 'Change Pin',
        icon: this.assetsLocation + 'change_pin.svg',
        route: ''
      },
      {
        name: 'Change Password',
        icon: this.assetsLocation + 'change_password.svg',
        route: ''
      },
      {
        name: 'Sign in with Face ID',
        icon: this.assetsLocation + 'face_ID.svg',
        route: ''
      }
    ];
  }

  initAboutItems() {
    this.aboutMenuItems = [
      {
        name: 'Terms and Conditions',
        icon: this.assetsLocation + 'terms_and_conditions.svg',
        route: ''
      },
      {
        name: 'Privacy Policy',
        icon: this.assetsLocation + 'privacy.svg',
        route: ''
      },
      {
        name: 'Follow us on Twitter',
        icon: this.assetsLocation + 'twitter.svg',
        route: ''
      },
      {
        name: 'Like us on Facebook',
        icon: this.assetsLocation + 'facebook.svg',
        route: ''
      },
    ];
  }
}
